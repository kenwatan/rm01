variable "region" {}

variable "compartment_ocid" {}

# Choose an Availability Domain
variable "availability_domain" {
  default = "2"
}

variable "instance_shape" {
  default = "VM.Standard2.1"
}

variable "ImageOS" {
  default = "Oracle Linux"
}

variable "ImageOSVersion" {
  default = "7.5"
}

variable "opc_user_name" {
  default = "opc"
}